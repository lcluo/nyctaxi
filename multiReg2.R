#install.packages("parallel")
library(parallel)
dyn.load("getDataMul.so")


getDatMul <- function(farefile, datafile){
  
  .Call("getDataMul", farefile, datafile)  
}

getlinearfun <- function(){
  
  f_data = list.files(pattern = "*data.*\\.csv$",)
  cl = makeCluster(12, "FORK")
  
  faredata = clusterApply(cl, f_data, function(datafile){ 
    farefile = gsub("data", "fare", datafile)      
    dat = getDatMul(farefile, datafile)
    dat 
  })
  
  sum = c(0, 0, 0, 0, 0, 0, 0, 0)
  
  sapply(faredata, function(x){
    sum[1] <<- sum[1]+x[1]+(sum[3]-x[3])*(sum[4]-x[4])*sum[5]*x[5]/(sum[5]+x[5])
    sum[2] <<- sum[2]+x[2]+(sum[3]-x[3])*(sum[3]-x[3])*sum[5]*x[5]/(sum[5]+x[5])
    sum[3] <<- (sum[3]*sum[5]+x[3]*x[5])/(sum[5]+x[5])
    sum[4] <<- (sum[4]*sum[5]+x[4]*x[5])/(sum[5]+x[5])
    sum[5] <<- sum[5]+x[5]
    sum[7] <<- sum[7]+x[7]+(sum[6]-x[6])*(sum[4]-x[4])*sum[5]*x[5]/(sum[5]+x[5])
    sum[8] <<- sum[8]+x[8]+(sum[6]-x[6])*(sum[6]-x[6])*sum[5]*x[5]/(sum[5]+x[5])
    sum[6] <<- (sum[6]*sum[5]+x[6]*x[5])/(sum[5]+x[5])
  })
  
  beta2 = sum[7]/sum[8]
  beta1 = sum[1]/sum[2]
  beta0 = sum[4] - beta1*sum[3] - beta2*sum[6]
  list(beta0 = beta0, beta1 = beta1, beta2 = beta2)
}