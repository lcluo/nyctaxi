#include <R.h>
#include <Rinternals.h>
#include <string.h>
#include <stdio.h>

SEXP getDataMul(SEXP farefile, SEXP datafile){
    const char *fare = CHAR(STRING_ELT(farefile, 0));
    const char *data = CHAR(STRING_ELT(datafile, 0));
    FILE *fp, *fq;
    char *line = NULL, *line2 = NULL;
    long farerow = 0;
    double mxold, mxnew, myold, mynew, covold, covnew, varold, varnew, n = 0;
    double mx2old, mx2new, cov2old, cov2new, var2old, var2new;

    SEXP ret;

    size_t len = 0,  len2 = 0;
    char delim[] = ",";
    fp = fopen(fare, "r");
    fq = fopen(data, "r");

    PROTECT(ret = allocMatrix(REALSXP, 1, 8));

    getline(&line, &len, fp);
    getline(&line2, &len2, fq);

    while( getline(&line, &len, fp) != -1){
        char *token, *id, *time;
        int i=0;
        double value, sec, sur;
        for (token = strsep(&line, delim); token != NULL; token = strsep(&line, delim)){
            i++;
            if (i==1) id = token; 
            if (i==4) time = token;
            if (i==7) sur = atof(token);
            if (i==10) value = -atof(token);
            if (i==11) value += atof(token);
        }

        while ( getline(&line2, &len2, fq) != -1 ){
            char *token2, *id2, *time2;
            i = 0;
            for (token2 = strsep(&line2, delim); token2 != NULL; token2 = strsep(&line2, delim)){
                i++;
                if (i==1) id2 = token2; 
                if (i==6) time2 = token2;
                if (i==9) sec = atof(token2);
            }
            if(strcmp(id, id2) == 0 & strcmp(time, time2) == 0) {
                if (n == 0){
                    mxold = sec;
                    varold = 0;
                    myold = value;
                    covold = 0;
                    mx2old = sur;
                    cov2old = 0;
                    var2old = 0;
                } else {
                    mxnew = mxold + (sec-mxold)/(n+1);
                    mynew = myold + (value-myold)/(n+1);
                    covnew = covold + (sec-mxnew)*(value-myold);
                    varnew = varold + (sec-mxnew)*(sec-mxold);
                    mx2new = mx2old + (sur-mx2old)/(n+1);
                    cov2new = cov2old + (sur-mx2new)*(value-myold);
                    var2new = var2old + (sur-mx2new)*(sur-mx2old);
                    printf("%g %g\n", value, myold);
                    mxold = mxnew;
                    myold = mynew;
                    covold = covnew;
                    varold = varnew;
                    mx2old = mx2new;
                    cov2old = cov2new;
                    var2old = var2new;
                }
                n = n + 1;
                break;
            }
        }
        farerow += 1;
    }
    REAL(ret)[0] = covold;
    REAL(ret)[1] = varold;
    REAL(ret)[2] = mxold;
    REAL(ret)[3] = myold;
    REAL(ret)[4] = n;
    REAL(ret)[5] = mx2old;
    REAL(ret)[6] = cov2old;
    REAL(ret)[7] = var2old;

    UNPROTECT(1);
    fclose(fq);
    fclose(fp);

    return ret;
}

