#include <R.h>
#include <Rinternals.h>
#include <string.h>
#include <stdio.h>

SEXP getData(SEXP farefile, SEXP datafile){
    const char *fare = CHAR(STRING_ELT(farefile, 0));
    const char *data = CHAR(STRING_ELT(datafile, 0));
    FILE *fp, *fq;
    char *line = NULL, *line2 = NULL;
    long farerow = 0;
    double mxold, mxnew, myold, mynew, covold, covnew, varold, varnew, n = 0;

    SEXP ret;

    size_t len = 0,  len2 = 0;
    char delim[] = ",";
    fp = fopen(fare, "r");
    fq = fopen(data, "r");

    PROTECT(ret = allocMatrix(REALSXP, 1, 5));

    getline(&line, &len, fp);
    getline(&line2, &len2, fq);

    while( getline(&line, &len, fp) != -1){
        char *token, *id, *time;
        int i=0;
        double value, sec;
        for (token = strsep(&line, delim); token != NULL; token = strsep(&line, delim)){
            i++;
            if (i==1) id = token; 
            if (i==4) time = token;
            if (i==10) value = -atof(token);
            if (i==11) value += atof(token);
        }

        while ( getline(&line2, &len2, fq) != -1 ){
            char *token2, *id2, *time2;
            i = 0;
            for (token2 = strsep(&line2, delim); token2 != NULL; token2 = strsep(&line2, delim)){
                i++;
                if (i==1) id2 = token2; 
                if (i==6) time2 = token2;
                if (i==9) sec = atof(token2);
            }
            if(strcmp(id, id2) == 0 & strcmp(time, time2) == 0) {
                if (n == 0){
                    mxold = sec;
                    varold = 0;
                    myold = value;
                    covold = 0;
                } else {
                    mxnew = mxold + (sec-mxold)/(n+1);
                    mynew = myold + (value-myold)/(n+1);
                    covnew = covold + (sec-mxnew)*(value-myold);
                    varnew = varold + (sec-mxnew)*(sec-mxold);
                    mxold = mxnew;
                    myold = mynew;
                    covold = covnew;
                    varold = varnew;
                }
                n = n + 1;
                break;
            }
        }
        farerow += 1;
    }
    REAL(ret)[0] = covold;
    REAL(ret)[1] = varold;
    REAL(ret)[2] = mxold;
    REAL(ret)[3] = myold;
    REAL(ret)[4] = n;

    UNPROTECT(1);
    fclose(fq);
    fclose(fp);

    return ret;
}
